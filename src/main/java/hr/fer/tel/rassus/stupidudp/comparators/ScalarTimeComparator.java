package hr.fer.tel.rassus.stupidudp.comparators;

import hr.fer.tel.rassus.stupidudp.Packet;

import java.util.Comparator;

public class ScalarTimeComparator implements Comparator<Packet> {
    @Override
    public int compare(Packet o1, Packet o2) {
        return Long.compare(o1.getScalarTime(), o2.getScalarTime());
    }
}
