package hr.fer.tel.rassus.stupidudp.kafka;

import hr.fer.tel.rassus.stupidudp.json.KafkaJsonDeserializer;
import hr.fer.tel.rassus.stupidudp.json.KafkaJsonSerializer;
import hr.fer.tel.rassus.stupidudp.sensor.Sensor;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Collections;
import java.util.Properties;
import java.util.UUID;

public  class Factory {

    public static Producer<String, Sensor> getTopicRegisterProducer(){
        Properties producerProperties = new Properties();
        producerProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        return new KafkaProducer<String, Sensor>(producerProperties, new StringSerializer(), new KafkaJsonSerializer());
    }

    public static Consumer<String, Sensor> getTopicRegisterConsumer() {
        Properties consumerProperties = new Properties();
        consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString());
        consumerProperties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        Consumer<String, Sensor> consumer =
                new org.apache.kafka.clients.consumer.KafkaConsumer<String, Sensor>(
                    consumerProperties,
                    new StringDeserializer(),
                    new KafkaJsonDeserializer<>(Sensor.class));
        consumer.subscribe(Collections.singleton("Register"));
        return consumer;
    }

    public static Consumer<String, String> getTopicCommandConsumer() {
        Properties consumerProperties = new Properties();
        consumerProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        consumerProperties.put(ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString());
        consumerProperties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        consumerProperties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        Consumer<String, String> consumer =
                new org.apache.kafka.clients.consumer.KafkaConsumer<String, String>(
                    consumerProperties,
                    new StringDeserializer(),
                    new StringDeserializer());
        consumer.subscribe(Collections.singleton("Command"));
        return consumer;
    }
}
