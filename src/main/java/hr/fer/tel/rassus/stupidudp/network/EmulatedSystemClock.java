/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package hr.fer.tel.rassus.stupidudp.network;

import java.util.Map;
import java.util.Random;

import static hr.fer.tel.rassus.stupidudp.SensorNodeMain.scalarTime;
import static hr.fer.tel.rassus.stupidudp.SensorNodeMain.vectorTime;

public class EmulatedSystemClock {

    private long startTime;
    private double jitter; //jitter per second,  percentage of deviation per 1 second

    public EmulatedSystemClock() {
        startTime = System.currentTimeMillis();
        Random r = new Random();
        jitter = (r.nextInt(20)) / 100d; //divide by 10 to get the interval between [0, 20], and then divide by 100 to get percentage
    }

    public long currentTimeMillis() {
        long current = System.currentTimeMillis();
        long diff = current - startTime;
        double coef = diff / 1000;

        return startTime + Math.round(diff * Math.pow((1 + jitter), coef));
    }


    public static void updateScalarTime(long receivedScalarTime) {
        if (receivedScalarTime > scalarTime) {
            scalarTime = receivedScalarTime + 1;
        } else {
            scalarTime++;
        }
    }


    public static void updateVectorTime(int myPort, Map<Integer, Integer> receivedVectorTime) {
        vectorTime.put(myPort, vectorTime.get(myPort) + 1);

        for (Integer port : receivedVectorTime.keySet()) {
            if (!port.equals(myPort)) {
                if (vectorTime.get(port) < receivedVectorTime.get(port)) {
                    vectorTime.put(port, receivedVectorTime.get(port));
                }
            }
        }
    }
}