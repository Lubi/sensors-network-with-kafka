package hr.fer.tel.rassus.stupidudp;

import hr.fer.tel.rassus.stupidudp.client.StupidUDPClient;
import hr.fer.tel.rassus.stupidudp.comparators.ScalarTimeComparator;
import hr.fer.tel.rassus.stupidudp.comparators.VectorTimeComparator;
import hr.fer.tel.rassus.stupidudp.kafka.Factory;
import hr.fer.tel.rassus.stupidudp.network.EmulatedSystemClock;
import hr.fer.tel.rassus.stupidudp.sensor.Sensor;
import hr.fer.tel.rassus.stupidudp.sensor.SensorReadings;
import hr.fer.tel.rassus.stupidudp.server.StupidUDPServer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.FileNotFoundException;
import java.time.Duration;
import java.util.*;

public class SensorNodeMain {
    public static long scalarTime = new EmulatedSystemClock().currentTimeMillis();
    public static Map<Integer, Integer> vectorTime = new HashMap<>(); // key is sensor id, value is logical time
    public static List<Packet> allPackets = new ArrayList<>();
    public static List<Packet> packetsFromLast5Seconds = new ArrayList<>();

    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        var scanner = new Scanner(System.in);

        var csv = new SensorReadings("readings.csv");
        String id = getFromInput("Write out id for sensor!", scanner);
        String myPort = getFromInput("Write out port for sensor!", scanner);
        var sensor = new Sensor(id, "localhost", myPort, csv);


        var topicCommandConsumer = Factory.getTopicCommandConsumer();
        waitForCommand(topicCommandConsumer, "Start");

        //when receive start command register myself on kafka server
        var producer = Factory.getTopicRegisterProducer();
        ProducerRecord<String, Sensor> record = new ProducerRecord<>("Register", null, sensor);
        producer.send(record);
        producer.flush();

        //wait some period to be secure that all nodes are registered
        Thread.sleep(1500);

        var topicRegisterConsumer = Factory.getTopicRegisterConsumer();
        ConsumerRecords<String, Sensor> consumerRecords = topicRegisterConsumer.poll(Duration.ofMillis(1000));
        List<Integer> allNodePorts = new ArrayList<>();
        consumerRecords.forEach(nodeRegisterRecord -> {
            var registeredSensor = (Sensor) nodeRegisterRecord.value();
            allNodePorts.add(Integer.valueOf(registeredSensor.getPort()));
        });
        allNodePorts.forEach(portTmp -> vectorTime.put(portTmp, 0));
        allNodePorts.remove(Integer.valueOf(myPort));

        var threads = startSensorNode(sensor, allNodePorts);

        waitForCommand(topicCommandConsumer, "Stop");
        threads.forEach(Thread::stop);

        //when receive stop command sort all readings
        System.out.println("*********ALL PACKETS**********");
        System.out.println("---------SORTED BY SCALAR TIME----------");
        sortAndPrint(allPackets, new ScalarTimeComparator());
        System.out.println("---------SORTED BY VECTOR TIME----------");
        sortAndPrint(allPackets, new VectorTimeComparator(Integer.valueOf(myPort)));
    }

    private static void waitForCommand(Consumer<String, String> topicCommandConsumer, String command) {
        boolean commandNotSent = true;
        while (commandNotSent) {
            var consumerRecords = topicCommandConsumer.poll(Duration.ofMillis(1000));
            if (!consumerRecords.isEmpty()) {
                for (var recordTmp : consumerRecords) {
                    if (recordTmp.value().equals(command)) {
                        commandNotSent = false;
                        break;
                    }
                }
            }
        }
    }

    private static List<Thread> startSensorNode(Sensor sensor, List<Integer> neighbourPorts) {
        int myPort = Integer.parseInt(sensor.getPort());
        List<Thread> threadsToStop = new ArrayList<>();
        // client thread
        var clientThread = new Thread(
                () -> {
                    while (true) {
                        //generate packet
                        //create new udp client with new packet
                        var reading = sensor.generateReading();

                        for (Integer neighbourPort : neighbourPorts) {
                            scalarTime++;
                            vectorTime.put(myPort, vectorTime.get(myPort) + 1);
                            var packet = new Packet(Integer.valueOf(reading.getCo()),
                                    scalarTime,
                                    vectorTime,
                                    myPort);
                            allPackets.add(packet);
                            synchronized (packetsFromLast5Seconds) {
                                packetsFromLast5Seconds.add(packet);
                            }
                            var threadForUDPClient = new Thread(() -> StupidUDPClient.start(myPort, neighbourPort, packet));
                            threadsToStop.add(threadForUDPClient);
                            threadForUDPClient.start();
                        }

                        try {
                            Thread.sleep(2500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );


        //server thread
        var serverThread = new Thread(() -> {
            //start udp server
            StupidUDPServer.start(myPort);
        });


        // thread for sort and mean every 5 seconds
        var scalarComparator = new ScalarTimeComparator();
        var vectorComparator = new VectorTimeComparator(myPort);
        var jobThread = new Thread(() -> {
            try {
                while (true) {
                    Thread.sleep(5000);
                    synchronized (packetsFromLast5Seconds) {
                        System.out.println("Datagrams sorted by SCALAR TIME");
                        sortAndPrint(packetsFromLast5Seconds, scalarComparator);
                        System.out.println("Datagrams sorted by VECTOR TIME");
                        sortAndPrint(packetsFromLast5Seconds, vectorComparator);

                        var mean = packetsFromLast5Seconds.stream().mapToInt(Packet::getCo).average().orElse(0);
                        System.out.println("Average co value in last 5 seconds is " + mean);
                        System.out.println();
                        System.out.println();
                        System.out.flush();
                        packetsFromLast5Seconds.clear();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        });

        var mainThreads = List.of(clientThread, serverThread, jobThread);
        mainThreads.forEach(Thread::start);

        threadsToStop.addAll(mainThreads);
        return threadsToStop;
    }

    private static void sortAndPrint(List<Packet> packets, Comparator<Packet> comparator) {
        packets.sort(comparator);
        packets.forEach(System.out::println);
        System.out.println();
        System.out.flush();
    }

    private static String getFromInput(String message, Scanner scanner) {
        System.out.println(message);
        return scanner.nextLine();
    }
}
