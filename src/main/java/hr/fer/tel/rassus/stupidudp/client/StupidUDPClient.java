/*
 * This code has been developed at Departement of Telecommunications,
 * Faculty of Electrical Eengineering and Computing, University of Zagreb.
 */
package hr.fer.tel.rassus.stupidudp.client;

import hr.fer.tel.rassus.stupidudp.Packet;
import hr.fer.tel.rassus.stupidudp.network.SimpleSimulatedDatagramSocket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static hr.fer.tel.rassus.stupidudp.SensorNodeMain.scalarTime;
import static hr.fer.tel.rassus.stupidudp.SensorNodeMain.vectorTime;

public class StupidUDPClient {

    public static void start(int myPort, int neighbourPort, Packet packet) {
        try {
            byte[] rcvBuf = new byte[256]; // received bytes
            byte[] sendBuf;

            InetAddress address = InetAddress.getByName("localhost");
            DatagramSocket socket = new SimpleSimulatedDatagramSocket(0.3, 1000); //SOCKET

            //send reading
            sendBuf = serializePacket(packet);
            var datagram = new DatagramPacket(sendBuf, sendBuf.length, address, neighbourPort);
            socket.send(datagram);
            System.out.println("Client send " + packet.getScalarTime() + " to port " + neighbourPort);
            System.out.flush();

            // wait for confirmation
            while (true) {
                // create a datagram packet for receiving data
                DatagramPacket rcvPacket = new DatagramPacket(rcvBuf, rcvBuf.length);
                try {
                    // receive a datagram packet from this socket
                    socket.receive(rcvPacket); //RECVFROM
                    vectorTime.put(myPort, vectorTime.get(myPort) + 1);
                    scalarTime++;
                    break;
                } catch (SocketTimeoutException e) {
                    // retransmission
                    socket.send(datagram);
                    System.out.println("Retransmission of packet " + packet.getScalarTime() + "to port " + neighbourPort);
                    System.out.flush();
                } catch (IOException ex) {
                    Logger.getLogger(StupidUDPClient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static byte[] serializePacket(Packet packet) {
        // Serialize to a byte array
        byte[] serializedMessage = new byte[0];
        try {
            ByteArrayOutputStream bStream = new ByteArrayOutputStream();
            ObjectOutput oo = new ObjectOutputStream(bStream);
            oo.writeObject(packet);
            oo.close();
            serializedMessage = bStream.toByteArray();
            return serializedMessage;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return serializedMessage;
    }
}

