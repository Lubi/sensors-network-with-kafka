package hr.fer.tel.rassus.stupidudp.sensor;


import com.opencsv.bean.CsvToBeanBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class SensorReadings {
    List<SensorReadingString> sensorReadingStrings;

    public SensorReadings(String fileName) throws FileNotFoundException {
        this.sensorReadingStrings = new CsvToBeanBuilder<SensorReadingString>(new FileReader(fileName))
                .withType(SensorReadingString.class).build().parse();
    }

    public SensorReadingString generateReading(LocalDateTime sensorCreatedDateTime) {
        long elapsedSecondsFromCreate = sensorCreatedDateTime.until(LocalDateTime.now(), ChronoUnit.SECONDS);
        int index = (int) (elapsedSecondsFromCreate % 100);
        return sensorReadingStrings.get(index);
    }

}
