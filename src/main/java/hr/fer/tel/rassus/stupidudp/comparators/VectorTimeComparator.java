package hr.fer.tel.rassus.stupidudp.comparators;

import hr.fer.tel.rassus.stupidudp.Packet;

import java.util.Comparator;

public class VectorTimeComparator implements Comparator<Packet> {
    private Integer myPort;

    public VectorTimeComparator(Integer myPort) {
        this.myPort = myPort;
    }

    @Override
    public int compare(Packet o1, Packet o2) {
        return Integer.compare(o1.getVectorTime().get(myPort), o2.getVectorTime().get(myPort));
    }
}
