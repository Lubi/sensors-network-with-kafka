/*
 * This code has been developed at Departement of Telecommunications,
 * Faculty of Electrical Engineering and Computing, University of Zagreb.
 */
package hr.fer.tel.rassus.stupidudp.server;

import hr.fer.tel.rassus.stupidudp.Packet;
import hr.fer.tel.rassus.stupidudp.network.EmulatedSystemClock;
import hr.fer.tel.rassus.stupidudp.network.SimpleSimulatedDatagramSocket;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import static hr.fer.tel.rassus.stupidudp.SensorNodeMain.*;

public class StupidUDPServer {

    public static void start(int myPort) {

        try {
            byte[] rcvBuf = new byte[512]; // received bytes
            byte[] sendBuf;

            DatagramSocket socket = new SimpleSimulatedDatagramSocket(myPort, 0.3, 1000); //SOCKET -> BIND

            while (true) {
                DatagramPacket packet = new DatagramPacket(rcvBuf, rcvBuf.length);
                // receive packet
                socket.receive(packet); //RECVFROM
                // deserialize packet
                ObjectInputStream iStream = new ObjectInputStream(new ByteArrayInputStream(packet.getData()));
                Packet receivedPacket = (Packet) iStream.readObject();
                iStream.close();

                if (!allPackets.contains(receivedPacket)) {
                    EmulatedSystemClock.updateScalarTime(receivedPacket.getScalarTime());
                    EmulatedSystemClock.updateVectorTime(myPort, receivedPacket.getVectorTime());
                    receivedPacket.setScalarTime(scalarTime);
                    receivedPacket.setVectorTime(new HashMap<>(vectorTime));

                    allPackets.add(receivedPacket);
                    synchronized (packetsFromLast5Seconds) {
                        packetsFromLast5Seconds.add(receivedPacket);
                    }
                }

                System.out.println("Server received: " + receivedPacket.getScalarTime() + " from port " + receivedPacket.getSenderPort());
                System.out.flush();

                var confirmationMessage = "Confirmation. Scalar time value = " + receivedPacket.getScalarTime();
                sendBuf = confirmationMessage.getBytes(StandardCharsets.UTF_8);
                // create a DatagramPacket for sending CONFIRMATION
                DatagramPacket sendPacket = new DatagramPacket(sendBuf,
                        sendBuf.length, packet.getAddress(), packet.getPort());

                // send confirmation message
                socket.send(sendPacket);
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
