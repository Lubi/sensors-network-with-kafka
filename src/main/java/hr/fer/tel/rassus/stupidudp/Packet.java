package hr.fer.tel.rassus.stupidudp;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Packet implements Serializable {
    private Integer co;
    private long scalarTime;
    private Map<Integer, Integer> vectorTime;
    private Integer senderPort;

    public Packet(Integer co, long scalarTime, Map<Integer, Integer> vectorTime, Integer senderPort) {
        this.co = co;
        this.scalarTime = scalarTime;
        this.vectorTime = new HashMap<>(vectorTime);
        this.senderPort = senderPort;
    }

    public Integer getSenderPort() {
        return senderPort;
    }

    public void setSenderPort(Integer senderPort) {
        this.senderPort = senderPort;
    }

    public Integer getCo() {
        return co;
    }

    public void setCo(Integer co) {
        this.co = co;
    }

    public long getScalarTime() {
        return scalarTime;
    }

    public void setScalarTime(long scalarTime) {
        this.scalarTime = scalarTime;
    }

    public Map<Integer, Integer> getVectorTime() {
        return vectorTime;
    }

    public void setVectorTime(Map<Integer, Integer> vectorTime) {
        this.vectorTime = vectorTime;
    }

    @Override
    public String toString() {
        return "Packet{" +
                "co=" + co +
                ", scalarTime=" + scalarTime +
                ", vectorTime=" + vectorTime.toString() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Packet packet = (Packet) o;
        return scalarTime == packet.scalarTime && Objects.equals(co, packet.co);
    }

    @Override
    public int hashCode() {
        return Objects.hash(co, scalarTime);
    }
}
