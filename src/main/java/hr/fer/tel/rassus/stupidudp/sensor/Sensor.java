package hr.fer.tel.rassus.stupidudp.sensor;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalDateTime;

public class Sensor {
    private String id;
    private String address;
    private String port;
    @JsonIgnore
    private LocalDateTime createdDateTime;
    @JsonIgnore
    private SensorReadings sensorReadings;

    public Sensor(String id, String address, String port, SensorReadings sensorReadings) {
        this.id = id;
        this.address = address;
        this.port = port;
        this.sensorReadings = sensorReadings;
        this.createdDateTime = LocalDateTime.now();
    }

    public Sensor() {
    }


    public void setAddress(String address) {
        this.address = address;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public void setSensorReadings(SensorReadings sensorReadings) {
        this.sensorReadings = sensorReadings;
    }


    public SensorReadingString generateReading() {
        return sensorReadings.generateReading(createdDateTime);
    }


    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }


    public String getAddress() {
        return address;
    }

    public String getPort() {
        return port;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public SensorReadings getSensorReadings() {
        return sensorReadings;
    }
}
