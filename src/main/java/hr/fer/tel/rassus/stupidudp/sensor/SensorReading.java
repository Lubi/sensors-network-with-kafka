package hr.fer.tel.rassus.stupidudp.sensor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SensorReading<Type> {
    private Type temperature, pressure, humidity, co, no2, so2;
    @JsonIgnore
    private long id;

    public SensorReading() {}

    public SensorReading(Type temperature, Type pressure, Type humidity, Type co, Type no2, Type so2) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.co = co;
        this.no2 = no2;
        this.so2 = so2;
    }

    public void setTemperature(Type temperature) {
        this.temperature = temperature;
    }

    public void setPressure(Type pressure) {
        this.pressure = pressure;
    }

    public void setHumidity(Type humidity) {
        this.humidity = humidity;
    }

    public void setCo(Type co) {
        this.co = co;
    }

    public void setNo2(Type no2) {
        this.no2 = no2;
    }

    public void setSo2(Type so2) {
        this.so2 = so2;
    }

    public Type getTemperature() {
        return temperature;
    }

    public Type getPressure() {
        return pressure;
    }

    public Type getHumidity() {
        return humidity;
    }

    public Type getCo() {
        return co;
    }

    public Type getNo2() {
        return no2;
    }

    public Type getSo2() {
        return so2;
    }

    @Override
    public String toString() {
        return "SensorReading{" +
                "temperature=" + temperature +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", co=" + co +
                ", no2=" + no2 +
                ", so2=" + so2 +
                '}';
    }
}
